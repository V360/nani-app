package eu.lepiller.nani;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentContainerView;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import androidx.viewpager2.widget.ViewPager2;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SearchView;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.moji4j.MojiConverter;
import com.moji4j.MojiDetector;

import java.util.ArrayList;
import java.util.List;

import eu.lepiller.nani.dictionary.DictionaryException;
import eu.lepiller.nani.dictionary.DictionaryFactory;
import eu.lepiller.nani.dictionary.IncompatibleFormatException;
import eu.lepiller.nani.dictionary.NoDictionaryException;
import eu.lepiller.nani.dictionary.NoResultDictionaryException;
import eu.lepiller.nani.result.ExampleResult;
import eu.lepiller.nani.result.KanjiResult;
import eu.lepiller.nani.result.Result;

import static java.lang.Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS;
import static eu.lepiller.nani.RadicalSelectorFragment.*;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private LinearLayout result_layout;
    private TextView feedback_text;
    private SearchView search_form;
    private FragmentContainerView radical_selector;
    private String readingStyle = "furigana";
    private String pitchStyle = "box";
    private String intentSearch = null;
    ViewPager2 viewPager2;
    ResultPagerAdapter pagerAdapter;

    private SearchThread searchThread;

    static final String TAG = "MAIN";

    // At this point we really need to redraw pagerAdapter, so suppress the warning.
    @SuppressLint("NotifyDataSetChanged")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        result_layout = findViewById(R.id.result_layout);
        feedback_text = findViewById(R.id.feedback);
        radical_selector = findViewById(R.id.radical_selector);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        viewPager2 = findViewById(R.id.pager);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        SharedPreferences sharedPref =
                PreferenceManager.getDefaultSharedPreferences(this);
        sharedPref.registerOnSharedPreferenceChangeListener(this);
        int radSizePref = getRadSizePref(sharedPref);
        readingStyle = getReadingStylePref(sharedPref);
        pitchStyle = getPitchStylePref(sharedPref);

        Button radical_button = findViewById(R.id.radical_button);

        pagerAdapter = new ResultPagerAdapter(this);
        pagerAdapter.setReadingStyle(readingStyle);
        pagerAdapter.setPitchStyle(pitchStyle);
        pagerAdapter.notifyDataSetChanged();
        viewPager2.setAdapter(pagerAdapter);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_results));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_kanji));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_example));

        new TabLayoutMediator(tabLayout, viewPager2,
                (tab, position) -> {
                    switch(position) {
                        case 0:
                            tab.setText(R.string.tab_results);
                            break;
                        case 1:
                            tab.setText(R.string.tab_kanji);
                            break;
                        case 2:
                            tab.setText(R.string.tab_example);
                            break;
                    }
                }
        ).attach();

        try {
            RadicalSelectorFragment.RadicalSelectorViewModel viewModel = new ViewModelProvider(this).get(RadicalSelectorFragment.RadicalSelectorViewModel.class);
            viewModel.setPreferences(new RadicalSelectorProperties(DictionaryFactory.getRadicalDictionary(getApplicationContext()), radSizePref));
        } catch (NoDictionaryException e) {
            e.printStackTrace();
        }

        radical_button.setOnClickListener(v -> {
            try {
                DictionaryFactory.getRadicalDictionary(getApplicationContext());
                radical_selector.setVisibility(View.VISIBLE);
                result_layout.setVisibility(View.INVISIBLE);
                search_form.setIconified(false);
            } catch (NoDictionaryException e) {
                Snackbar.make(search_form, getString(R.string.no_radical_dict), Snackbar.LENGTH_LONG).show();
            }
        });

        getSupportFragmentManager().setFragmentResultListener("selectRadicals", this, (requestKey, bundle) -> {
            int event = bundle.getInt("event");
            Log.d(TAG, "received result: " + event);
            if(event == CLOSE_EVENT) {
                closeRadicals();
            } else if(event == HELP_EVENT) {
                Intent intent = new Intent(MainActivity.this, HelpRadicalActivity.class);
                startActivity(intent);
            } else if(event == KANJI_EVENT) {
                String k = bundle.getString("kanji");
                search_form.setQuery(search_form.getQuery() + k, false);
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            CharSequence searchTerm = getIntent().getCharSequenceExtra(Intent.EXTRA_PROCESS_TEXT);
            if(searchTerm != null && !searchTerm.toString().isEmpty())
                intentSearch = searchTerm.toString();
        }
    }

    private void closeRadicals() {
        radical_selector.setVisibility(View.INVISIBLE);
        result_layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.d("MAIN", key + " preference changed.");
        if(key.compareTo(SettingsActivity.KEY_PREF_RAD_SIZE) == 0) {
            RadicalSelectorFragment.RadicalSelectorViewModel viewModel = new ViewModelProvider(this).get(RadicalSelectorFragment.RadicalSelectorViewModel.class);
            viewModel.setPreferences(new RadicalSelectorProperties(null, getRadSizePref(sharedPreferences)));
        } else if(key.compareTo(SettingsActivity.KEY_PREF_READING_STYLE) == 0) {
            readingStyle = getReadingStylePref(sharedPreferences);
            pagerAdapter.setReadingStyle(readingStyle);
            pagerAdapter.notifyItemChanged(0);
        } else if(key.compareTo(SettingsActivity.KEY_PREF_PITCH_STYLE) == 0) {
            pitchStyle = getPitchStylePref(sharedPreferences);
            pagerAdapter.setPitchStyle(pitchStyle);
            pagerAdapter.notifyItemChanged(0);
        }
    }

    private int getRadSizePref(SharedPreferences sharedPrefs) {
        String prefRadSize = sharedPrefs.getString(SettingsActivity.KEY_PREF_RAD_SIZE, "122");
        return Integer.parseInt(prefRadSize);
    }

    private String getReadingStylePref(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(SettingsActivity.KEY_PREF_READING_STYLE, "furigana");
    }

    private String getPitchStylePref(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(SettingsActivity.KEY_PREF_PITCH_STYLE, "box");
    }

    private class SearchThread extends Thread {
        String text;

        SearchThread(String text) {
            this.text = text;
        }

        @Override
        public void run() {
            SearchResult res = findResults();
            runOnUiThread(() -> {
                stopSearchThread();
                showResults(res);
            });
        }

        private SearchResult findResults() {
            boolean converted = false;
            ArrayList<String> tried = new ArrayList<>();
            tried.add(text);

            ArrayList<Result> searchResult;
            try {
                searchResult = DictionaryFactory.search(text);
            } catch (DictionaryException e) {
                return new SearchResult(e);
            }

            if(searchResult.isEmpty()) {
                MojiConverter converter = new MojiConverter();
                try {
                    tried.add(converter.convertRomajiToHiragana(text));
                    searchResult = DictionaryFactory.search(converter.convertRomajiToHiragana(text));
                    searchResult.addAll(DictionaryFactory.search(converter.convertRomajiToKatakana(text)));
                    converted = true;
                } catch (DictionaryException e) {
                    return new SearchResult(e);
                }
            }

            List<String> kanji = new ArrayList<>();
            for(String c: text.split("")) {
                if(c.isEmpty()) {
                    Log.w(TAG, "Splitting text " + text + "gave an empty component");
                } else if (Character.UnicodeBlock.of(c.codePointAt(0)) == CJK_UNIFIED_IDEOGRAPHS) {
                    kanji.add(c);
                }
            }
            if(kanji.isEmpty()) {
                int count = 0;
                for(Result r: searchResult) {
                    for(String alt: r.getAlternatives()) {
                        for(String c: alt.split("")) {
                            if(count>5)
                                break;
                            if(!c.isEmpty() && Character.UnicodeBlock.of(c.codePointAt(0)) == CJK_UNIFIED_IDEOGRAPHS
                                && !kanji.contains(c)) {
                                count++;
                                kanji.add(c);
                            }
                        }
                    }
                }
            }

            List<KanjiResult> kanjiResults = new ArrayList<>();
            for(String c: kanji) {
                KanjiResult r;
                try {
                    r = DictionaryFactory.searchKanji(c);
                } catch(DictionaryException e) {
                    break;
                }
                Log.d(TAG, "kanji " + c + ", result: " + r);
                if(r != null)
                    kanjiResults.add(r);
            }

            List<ExampleResult> exampleResults = new ArrayList<>();
            for(Result r: searchResult) {
                String word = r.getKanji();

                List<ExampleResult> res;
                try {
                    res = DictionaryFactory.searchExamples(word);
                } catch(DictionaryException e) {
                    break;
                }
                exampleResults.addAll(res);
            }

            if(searchResult.isEmpty() && kanjiResults.isEmpty() && exampleResults.isEmpty()) {
                return new SearchResult(new NoResultDictionaryException(tried));
            }

            return new SearchResult(searchResult, kanjiResults, exampleResults, text, converted);
        }
    }

    private synchronized void stopSearchThread() {
        this.searchThread = null;
    }

    public void showResults(SearchResult r) {
        stopSearchThread();
        search_form.setEnabled(true);

        if(r.isException()) {
            DictionaryException e = r.getException();
            if (e instanceof NoDictionaryException) {
                Snackbar.make(search_form, getString(R.string.no_dic),
                        Snackbar.LENGTH_LONG).show();
                feedback_text.setText(getString(R.string.no_dic));
            } else if(e instanceof IncompatibleFormatException) {
                Snackbar.make(search_form, String.format(getString(R.string.incompatible_format),
                        ((IncompatibleFormatException) e).getName()), Snackbar.LENGTH_LONG).show();
                feedback_text.setText(String.format(getString(R.string.incompatible_format),
                        ((IncompatibleFormatException) e).getName()));
            } else if(e instanceof NoResultDictionaryException) {
                StringBuilder sb = new StringBuilder();
                boolean first = true;
                for(String s: ((NoResultDictionaryException) e).getTried()) {
                    if(first) {
                        first = false;
                    } else {
                        sb.append(getText(R.string.sense_separator));
                    }
                    sb.append(s);
                }
                feedback_text.setText(String.format(getString(R.string.feedback_no_result_tried), sb));
            }
            return;
        }

        feedback_text.setText("");

        List<Result> searchResult = r.getResults();
        List<KanjiResult> kanjiResults = r.getKanjiResults();
        List<ExampleResult> exampleResults = r.getExampleResults();

        MojiDetector detector = new MojiDetector();
        if(searchResult != null && !searchResult.isEmpty() && !r.isConverted() && detector.hasRomaji(r.getText())) {
            MojiConverter converter = new MojiConverter();
            final String moji = converter.convertRomajiToHiragana(r.getText());
            feedback_text.setText(String.format(getString(R.string.feedback_didyoumean), moji));

            feedback_text.setOnClickListener(v -> {
                search_form.setQuery(moji, true);
                feedback_text.setOnClickListener(null);
            });
        }

        if(searchResult != null || kanjiResults != null) {
            if((searchResult == null || searchResult.isEmpty()) && kanjiResults.isEmpty() && exampleResults.isEmpty()) {
                feedback_text.setText(R.string.feedback_no_result);
            }
        }

        pagerAdapter.setKanjiResults(kanjiResults);
        pagerAdapter.setResults(searchResult);
        pagerAdapter.setExampleResults(exampleResults);
        pagerAdapter.notifyItemChanged(0);
        pagerAdapter.notifyItemChanged(1);
        pagerAdapter.notifyItemChanged(2);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        search_form = (SearchView) searchItem.getActionView();

        search_form.setQueryHint(getResources().getString(R.string.search_hint));

        search_form.setOnCloseListener(() -> {
            closeRadicals();
            return false;
        });

        search_form.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                radical_selector.setVisibility(View.INVISIBLE);
                result_layout.setVisibility(View.VISIBLE);

                String text = search_form.getQuery().toString();
                if (text.isEmpty()) {
                    Snackbar.make(search_form, getString(R.string.no_search), Snackbar.LENGTH_LONG).show();
                    return false;
                }

                text = text.replace("？", "?");

                pagerAdapter.setKanjiResults(new ArrayList<>());
                pagerAdapter.setResults(new ArrayList<>());
                pagerAdapter.notifyItemChanged(0);
                pagerAdapter.notifyItemChanged(1);
                search_form.setEnabled(false);
                feedback_text.setText(R.string.feedback_progress);

                DictionaryFactory.prepare(getApplicationContext());
                MainActivity.this.search(text);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private synchronized void search(String text) {
        if(searchThread == null) {
            searchThread = new SearchThread(text);
            searchThread.start();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if(intentSearch != null) {
            search_form.setQuery(intentSearch, true);
            intentSearch = null;
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_dictionaries) {
            Intent intent = new Intent(this, DictionaryActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_licenses) {
            Intent intent = new Intent(this, LicenseActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(radical_selector.getVisibility() == View.VISIBLE) {
            closeRadicals();
        } else {
            super.onBackPressed();
        }
    }
}
