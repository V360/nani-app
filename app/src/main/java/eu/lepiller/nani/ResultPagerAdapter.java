package eu.lepiller.nani;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.lepiller.nani.result.ExampleResult;
import eu.lepiller.nani.views.KanjiStrokeView;
import eu.lepiller.nani.views.PitchContourView;
import eu.lepiller.nani.views.PitchDiagramView;
import me.weilunli.views.RubyTextView;
import eu.lepiller.nani.result.KanjiResult;
import eu.lepiller.nani.result.Result;

public class ResultPagerAdapter extends RecyclerView.Adapter<ResultPagerAdapter.ViewHolder> {
    static List<Result> results = new ArrayList<>();
    static List<KanjiResult> kanjiResults = new ArrayList<>();
    static List<ExampleResult> exampleResults = new ArrayList<>();
    private static String readingStyle = "furigana";
    private static String pitchStyle = "box";
    private final Context context;

    static final String TAG = "RESULTS_PAGER";

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final LinearLayout result_view;
        private final Context context;

        ViewHolder(View view, Context context) {
            super(view);
            this.context = context;
            result_view = view.findViewById(R.id.results_view);

            Log.d(TAG, "createView");
        }

        final String getContent(List<String> content) {
            StringBuilder sb = new StringBuilder();
            boolean separator1 = false;
            for (String s : content) {
                if (separator1)
                    sb.append(context.getResources().getString(R.string.sense_separator));
                else
                    separator1 = true;
                sb.append(s);
            }
            return sb.toString();
        }

        void showExampleResults() {
            result_view.removeAllViews();
            if(exampleResults == null)
                return;

            Log.d(TAG, "(show) examples: " + exampleResults.size());

            for(ExampleResult result: exampleResults) {
                View child_result = LayoutInflater.from(context).inflate(R.layout.layout_example, result_view, false);

                TextView japaneseView = child_result.findViewById(R.id.japanese);
                TextView translationView = child_result.findViewById(R.id.translation);
                TextView langView = child_result.findViewById(R.id.lang_view);

                japaneseView.setText(result.getJapanese());
                translationView.setText(result.getOtherLang());
                langView.setText(result.getLang());

                result_view.addView(child_result);
            }
        }

        void showKanjiResults() {
            result_view.removeAllViews();
            if(kanjiResults == null)
                return;

            Log.d(TAG, "(show) kanjis: " + kanjiResults.size());

            for(KanjiResult result: kanjiResults) {
                View child_result = LayoutInflater.from(context).inflate(R.layout.layout_kanji, result_view, false);

                TextView kanjiView = child_result.findViewById(R.id.kanji_view);
                TextView strokesView = child_result.findViewById(R.id.kanji_strokes);
                TextView elementsView = child_result.findViewById(R.id.kanji_elements);
                KanjiStrokeView strokeView = child_result.findViewById(R.id.kanji_stroke_view);
                LinearLayout senses_view = child_result.findViewById(R.id.sense_view);
                TextView onView = child_result.findViewById(R.id.on_reading);
                TextView kunView = child_result.findViewById(R.id.kun_reading);
                TextView nanoriView = child_result.findViewById(R.id.nanori_reading);

                kanjiView.setText(result.getKanji());
                strokesView.setText(String.format(context.getResources().getQuantityString(R.plurals.kanji_stroke, result.getStroke()),
                        result.getStroke()));
                strokeView.setStrokes(result.getStrokes());
                if(result.getStrokes() == null)
                    strokeView.setVisibility(View.GONE);

                elementsView.setText(String.format(context.getResources().getQuantityString(R.plurals.kanji_elements, result.getElements().size()),
                        getContent(result.getElements())));
                if(result.getElements().isEmpty())
                    elementsView.setVisibility(View.GONE);

                senses_view.removeAllViews();
                Map<String, List<String>> meanings = new HashMap<>();
                for(KanjiResult.Sense sense: result.getSenses()) {
                    List<String> content = meanings.get(sense.getLang());
                    if(content == null)
                        content = new ArrayList<>();
                    content.add(sense.getContent());
                    meanings.put(sense.getLang(), content);
                }

                int sense_pos = 1;
                for(String lang: meanings.keySet()) {
                    View child = LayoutInflater.from(context).inflate(R.layout.layout_sense, senses_view, false);
                    TextView id_view = child.findViewById(R.id.id_view);
                    TextView lang_view = child.findViewById(R.id.lang_view);
                    TextView sense_view = child.findViewById(R.id.definition_view);

                    id_view.setText(String.format(context.getResources().getString(R.string.sense_number), sense_pos));
                    lang_view.setText(lang);

                    StringBuilder sb = new StringBuilder();
                    boolean separator1 = false;
                    List<String> langs = meanings.get(lang);
                    if(langs != null) {
                        for (String s : langs) {
                            if (separator1)
                                sb.append(context.getResources().getString(R.string.sense_separator));
                            else
                                separator1 = true;
                            sb.append(s);
                        }
                    }
                    sense_view.setText(Html.fromHtml(sb.toString()));

                    senses_view.addView(child);
                    sense_pos++;
                }
                kunView.setText(String.format(context.getResources().getString(R.string.kun_reading),
                        getContent(result.getKun())));
                onView.setText(String.format(context.getResources().getString(R.string.on_reading),
                        getContent(result.getOn())));
                nanoriView.setText(String.format(context.getResources().getString(R.string.nanori_reading),
                        getContent(result.getNanori())));

                result_view.addView(child_result);
            }
        }

        void showResults() {
            result_view.removeAllViews();
            if(results == null)
                return;

            for(Result result: results) {
                View child_result = LayoutInflater.from(context).inflate(R.layout.layout_result, result_view, false);

                RubyTextView kanji_view = child_result.findViewById(R.id.kanji_view);
                TextView reading_view = child_result.findViewById(R.id.reading_view);
                LinearLayout senses_view = child_result.findViewById(R.id.sense_view);
                TextView additional_info = child_result.findViewById(R.id.additional_info_view);
                TextView pitch_view = child_result.findViewById(R.id.pitch_view);
                PitchDiagramView pitch_diagram = child_result.findViewById(R.id.pitch_diagram_view);
                PitchContourView pitch_contour = child_result.findViewById(R.id.pitch_contour_view);

                // Populate the data into the template view using the data object
                if(readingStyle.compareTo("furigana") == 0) {
                    kanji_view.setCombinedText(result.getKanjiFurigana());
                    reading_view.setVisibility(View.GONE);
                } else {
                    kanji_view.setCombinedText(result.getKanji());
                    reading_view.setVisibility(View.VISIBLE);
                    reading_view.setText(readingStyle.compareTo("kana") == 0? result.getReading(): result.getRomajiReading());
                }

                // If pitch information is available, make it visible
                String pitch = result.getPitch();
                if(pitch != null && !pitch.isEmpty()) {
                    // Try to get rid of additional characters, by finding the first number.
                    // XXX: this means the result contains only the first pattern
                    StringBuilder number = new StringBuilder();
                    boolean foundFirst = false;
                    for(int i = 0; i<pitch.length(); i++) {
                        if(pitch.charAt(i) < '0' || pitch.charAt(i) > '9') {
                            if(foundFirst)
                                break;
                        } else {
                            foundFirst = true;
                            number.append(pitch.charAt(i));
                        }
                    }
                    String cleanPitch = number.toString();
                    int pitchInt = -1;
                    if(!cleanPitch.isEmpty())
                        pitchInt = Integer.parseInt(cleanPitch);

                    if(pitchStyle.compareTo("box") == 0) {
                        pitch_view.setVisibility(View.VISIBLE);
                        pitch_diagram.setVisibility(View.GONE);
                        pitch_contour.setVisibility(View.GONE);
                    } else if(pitchStyle.compareTo("diagram") == 0) {
                        pitch_contour.setVisibility(View.GONE);
                        pitch_view.setVisibility(View.GONE);
                        if(pitchInt >= 0) {
                            if (readingStyle.compareTo("kana") == 0) {
                                reading_view.setVisibility(View.GONE);
                            }
                            pitch_diagram.setVisibility(View.VISIBLE);
                            pitch_diagram.setText(result.getReading());
                            pitch_diagram.setPitch(pitchInt);
                        } else {
                            pitch_diagram.setVisibility(View.GONE);
                        }
                    } else if(pitchStyle.compareTo("contour") == 0) {
                        pitch_diagram.setVisibility(View.GONE);
                        pitch_view.setVisibility(View.GONE);
                        if(pitchInt >= 0) {
                            if (readingStyle.compareTo("kana") == 0) {
                                reading_view.setVisibility(View.GONE);
                            }
                            pitch_contour.setVisibility(View.VISIBLE);
                            pitch_contour.setText(result.getReading());
                            pitch_contour.setPitch(pitchInt);
                        } else {
                            pitch_contour.setVisibility(View.GONE);
                        }
                    }

                    pitch_view.setText(pitch);
                    pitch_view.setOnClickListener(v -> {
                        Intent intent = new Intent(context, HelpPitchActivity.class);
                        context.startActivity(intent);
                    });
                }

                if(result.getAlternatives().size() > 1)
                    additional_info.setText(String.format(context.getResources().getString(R.string.sense_alternatives), getContent(result.getAlternatives())));
                else
                    additional_info.setVisibility(View.GONE);

                senses_view.removeAllViews();

                int sense_pos = 1;
                for (Result.Sense sense : result.getSenses()) {
                    View child = LayoutInflater.from(context).inflate(R.layout.layout_sense, senses_view, false);
                    TextView id_view = child.findViewById(R.id.id_view);
                    TextView lang_view = child.findViewById(R.id.lang_view);
                    TextView sense_view = child.findViewById(R.id.definition_view);

                    id_view.setText(String.format(context.getResources().getString(R.string.sense_number), sense_pos));
                    lang_view.setText(sense.getLanguage());

                    StringBuilder sb = new StringBuilder();
                    sb.append("<font color=\"#909090\"><i>");
                    sb.append(getContent(sense.getInfos()));
                    sb.append("</i></font>");
                    if(sense.getInfos().size() > 0)
                        sb.append(" ");
                    sb.append(getContent(sense.getGlosses()));
                    sense_view.setText(Html.fromHtml(sb.toString()));

                    senses_view.addView(child);
                    sense_pos++;
                }

                result_view.addView(child_result);
            }
        }
    }

    private final LayoutInflater mInflater;
    ResultPagerAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_results, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch(position) {
            case 0:
                holder.showResults();
                break;
            case 1:
                holder.showKanjiResults();
                break;
            case 2:
                holder.showExampleResults();
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    // Changing the style requires redrawing everything, so suppress warning
    @SuppressLint("NotifyDataSetChanged")
    public void setReadingStyle(String readingStyle) {
        ResultPagerAdapter.readingStyle = readingStyle;
        Log.d(TAG, "reading style updated to " + readingStyle);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setPitchStyle(String pitchStyle) {
        ResultPagerAdapter.pitchStyle = pitchStyle;
        Log.d(TAG, "pitch style updated to " + pitchStyle);
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setExampleResults(List<ExampleResult> exampleResults) {
        ResultPagerAdapter.exampleResults.clear();
        if(exampleResults != null)
            ResultPagerAdapter.exampleResults.addAll(exampleResults);
        Log.d(TAG, "examples: " + ResultPagerAdapter.exampleResults.size());
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setKanjiResults(List<KanjiResult> kanjiResults) {
        ResultPagerAdapter.kanjiResults.clear();
        if(kanjiResults != null)
            ResultPagerAdapter.kanjiResults.addAll(kanjiResults);
        Log.d(TAG, "kanjis: " + ResultPagerAdapter.kanjiResults.size());
        notifyDataSetChanged();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setResults(List<Result> results) {
        ResultPagerAdapter.results.clear();
        if(results != null)
            ResultPagerAdapter.results.addAll(results);
        Log.d(TAG, "results: " + ResultPagerAdapter.results.size());
        notifyDataSetChanged();
    }
}
