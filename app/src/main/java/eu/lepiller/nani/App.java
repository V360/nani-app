package eu.lepiller.nani;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;

public class App extends Application {
    public static final String DICTIONARY_DOWNLOAD_NOTIFICATION_CHANNEL = "dictionaryDownloadChannel";

    @Override
    public void onCreate() {
        super.onCreate();

        createNotificationChannel();
    }

    private void createNotificationChannel() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(DICTIONARY_DOWNLOAD_NOTIFICATION_CHANNEL,
                    "Nani's dictionary download notification", NotificationManager.IMPORTANCE_DEFAULT);

            NotificationManager manager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if(manager != null)
                manager.createNotificationChannel(channel);
        }
    }
}
