package eu.lepiller.nani;

public interface OnOtherTaskCompleted<Result> {
    void onOtherTaskCompleted(Result result);
}
