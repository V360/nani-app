package eu.lepiller.nani;

import static android.view.View.TEXT_ALIGNMENT_CENTER;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayout;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.lepiller.nani.dictionary.IncompatibleFormatException;
import eu.lepiller.nani.dictionary.RadicalDict;

public class RadicalSelectorFragment extends Fragment {
    public static final int CLOSE_EVENT = 1;
    public static final int HELP_EVENT = 2;
    public static final int KANJI_EVENT = 3;

    private LinearLayout kanji_row1, kanji_row2;
    private LinearLayout radical_table;
    private ProgressBar radical_load;

    final List<String> selected = new ArrayList<>();
    final List<ToggleButton> buttons = new ArrayList<>();

    RadicalDict dictionary;
    private int radSize = 122;

    public RadicalSelectorFragment() {
        super(R.layout.content_radicals);
    }

    @Override
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);

        if (getView() == null)
            return;

        Button close_button = getView().findViewById(R.id.close_button);
        Button help_button = getView().findViewById(R.id.help_button);
        Button clear_button = getView().findViewById(R.id.clear_button);
        kanji_row1 = getView().findViewById(R.id.kanji_row1);
        kanji_row2 = getView().findViewById(R.id.kanji_row2);
        radical_table = getView().findViewById(R.id.radical_table);
        radical_load = getView().findViewById(R.id.radical_load);

        radical_load.setIndeterminate(true);
        if(dictionary == null) {
            radical_load.setVisibility(View.VISIBLE);
            kanji_row1.setVisibility(View.GONE);
            kanji_row2.setVisibility(View.GONE);
            radical_table.setVisibility(View.GONE);
        } else {
            radical_load.setVisibility(View.GONE);
            kanji_row1.setVisibility(View.VISIBLE);
            kanji_row2.setVisibility(View.VISIBLE);
            radical_table.setVisibility(View.VISIBLE);
        }

        close_button.setOnClickListener((View v) -> {
            Bundle result = new Bundle();
            result.putInt("event", CLOSE_EVENT);
            getParentFragmentManager().setFragmentResult("selectRadicals", result);
        });

        help_button.setOnClickListener((View v) -> {
            Bundle result = new Bundle();
            result.putInt("event", HELP_EVENT);
            getParentFragmentManager().setFragmentResult("selectRadicals", result);
        });

        clear_button.setOnClickListener((View v) -> {
            selected.clear();
            for(ToggleButton b: buttons)
                if(b.isChecked())
                    b.toggle();
            new Thread(dictionarySearch).start();
        });

        RadicalSelectorViewModel viewModel = new ViewModelProvider(requireActivity()).get(RadicalSelectorViewModel.class);
        viewModel.getPreferences().observe(getViewLifecycleOwner(), pref -> {
            if (pref.dictionary != null) {
                setDictionary(pref.dictionary);
            }
            if (pref.radSize != null) {
                setRadSize(radSize);
            }
        });
    }

    private final Runnable dictionarySearch = () -> {
        Pair<Map<Integer, List<String>>, Set<String>> result;
        try {
            result = new Pair<>(dictionary.match(selected), dictionary.availableRadicals(selected));
        } catch (IncompatibleFormatException e) {
            e.printStackTrace();
            return;
        }
        Activity activity = getActivity();
        if(activity == null)
            return;

        activity.runOnUiThread(() -> {
            Map<Integer, List<String>> kanji = result.first;
            Set<String> availableRadicals = result.second;

            updateSelection(kanji);
            updateRadicals(availableRadicals);
        });
    };

    private final Runnable dictionaryUpdate = () -> {
        Map<Integer, List<String>> radicals;
        try {
            radicals = dictionary.getAllRadicals();
        } catch (IncompatibleFormatException e) {
            e.printStackTrace();
            return;
        }

        Activity activity = getActivity();
        if(activity == null)
            return;

        activity.runOnUiThread(() -> createButtons(radicals));
    };

    public static class RadicalSelectorProperties {
        RadicalSelectorProperties(RadicalDict d, int s) {
            this.dictionary = d;
            this.radSize = s;
        }
        public RadicalDict dictionary;
        public Integer radSize;
    }

    public static class RadicalSelectorViewModel extends ViewModel {
        private final MutableLiveData<RadicalSelectorProperties> preferences = new MutableLiveData<>();
        public void setPreferences(RadicalSelectorProperties item) {
            preferences.setValue(item);
        }
        public LiveData<RadicalSelectorProperties> getPreferences() {
            return preferences;
        }
    }

    public void setDictionary(RadicalDict dict) {
        dictionary = dict;

        radical_load.setVisibility(View.VISIBLE);
        kanji_row1.setVisibility(View.GONE);
        kanji_row2.setVisibility(View.GONE);
        radical_table.setVisibility(View.GONE);

        new Thread(dictionaryUpdate).start();
    }

    public void setRadSize(int size) {
        radSize = size;
        resizeRadicals();
    }

    public int getRadFontSize() {
        return radSize / 8;
    }

    private void createButtons(Map<Integer, List<String>> radicals) {
        if(radicals == null)
            return;

        if(getContext() == null)
            return;

        ArrayList<Integer> strokes = new ArrayList<>(radicals.keySet());
        Collections.sort(strokes);
        for (int stroke : strokes) {
            TextView strokeView = new TextView(getContext());
            NumberFormat nf = NumberFormat.getInstance();
            strokeView.setText(nf.format(stroke));
            radical_table.addView(strokeView);

            FlexboxLayout box = new FlexboxLayout(getContext());
            radical_table.addView(box);

            List<String> lst = radicals.get(stroke);
            if(lst == null)
                continue;

            for (final String radical : lst) {
                ToggleButton radicalButton = new ToggleButton(getContext());
                radicalButton.setTextOff(radical);
                radicalButton.setTextOn(radical);
                radicalButton.setText(radical);
                radicalButton.setTextSize(getRadFontSize());
                radicalButton.setLayoutParams(new FlexboxLayout.LayoutParams(radSize, radSize));

                radicalButton.setOnClickListener(v -> {
                    boolean checked = ((ToggleButton) v).isChecked();
                    if (checked) {
                        selected.add(radical);
                    } else {
                        selected.remove(radical);
                    }

                    new Thread(dictionarySearch).start();
                });
                buttons.add(radicalButton);
                box.addView(radicalButton);
            }

            box.setFlexWrap(FlexWrap.WRAP);
            box.invalidate();
        }

        radical_load.setVisibility(View.GONE);
        kanji_row1.setVisibility(View.VISIBLE);
        kanji_row2.setVisibility(View.VISIBLE);
        radical_table.setVisibility(View.VISIBLE);
    }

    private void updateRadicals(Set<String> available) {
        Log.v("SELECTOR", Integer.toString(radical_table.getChildCount()));
        for(int row = 0; row < radical_table.getChildCount(); row++) {
            View r = radical_table.getChildAt(row);
            if(r instanceof FlexboxLayout) {
                for (int col = 0; col < ((FlexboxLayout) r).getChildCount(); col++) {
                    View child = ((FlexboxLayout) r).getChildAt(col);
                    if (child instanceof ToggleButton) {
                        child.setEnabled(available == null || available.contains(((ToggleButton) child).getText().toString()));
                    }
                }
            }
        }
    }

    public void updateSelection(Map<Integer, List<String>> kanji) {
        LinearLayout row = kanji_row1;
        kanji_row1.removeAllViews();
        kanji_row2.removeAllViews();

        if(kanji == null)
            return;

        int total = 0;
        int number = 0;
        for (int stroke : kanji.keySet()) {
            List<String> s = kanji.get(stroke);
            if(s != null)
                total += s.size();
        }
        for (int stroke : kanji.keySet()) {
            TextView strokeView = new TextView(getContext());
            NumberFormat nf = NumberFormat.getInstance();
            strokeView.setText(nf.format(stroke));
            strokeView.setLayoutParams(new LinearLayout.LayoutParams(66, 66));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(getContext() != null)
                    strokeView.setTextColor(getContext().getColor(R.color.colorAccent));
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                strokeView.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            }
            row.addView(strokeView);

            List<String> kanjis = kanji.get(stroke);
            if(kanjis == null)
                continue;

            for (String k : kanjis) {
                number++;
                Button kanji_button = new Button(getContext());
                kanji_button.setText(k);
                kanji_button.setOnClickListener((View v) -> {
                    Bundle result = new Bundle();
                    result.putInt("event", KANJI_EVENT);
                    result.putString("kanji", k);
                    getParentFragmentManager().setFragmentResult("selectRadicals", result);
                });
                kanji_button.setLayoutParams(new LinearLayout.LayoutParams(radSize, radSize));
                kanji_button.setTextSize(getRadFontSize());
                row.addView(kanji_button);

                if (number > total / 2)
                    row = kanji_row2;
            }
        }
    }

    private void resizeRadicals() {
        for(int row=0; row < radical_table.getChildCount(); row++) {
            View r = radical_table.getChildAt(row);
            if(r instanceof FlexboxLayout) {
                for(int col = 0; col < ((FlexboxLayout) r).getChildCount(); col++) {
                    View child = ((FlexboxLayout) r).getChildAt(col);
                    if(child instanceof ToggleButton) {
                        child.setLayoutParams(new FlexboxLayout.LayoutParams(radSize, radSize));
                        ((ToggleButton)child).setTextSize(getRadFontSize());
                    }
                }
            }
        }
        for(int kanji = 0; kanji < kanji_row1.getChildCount(); kanji++) {
            View k = kanji_row1.getChildAt(kanji);
            if(k instanceof Button) {
                ((Button) k).setTextSize(getRadFontSize());
                k.setLayoutParams(new LinearLayout.LayoutParams(radSize, radSize));
            }
        }
        for(int kanji = 0; kanji < kanji_row2.getChildCount(); kanji++) {
            View k = kanji_row2.getChildAt(kanji);
            if(k instanceof Button) {
                ((Button) k).setTextSize(getRadFontSize());
                k.setLayoutParams(new LinearLayout.LayoutParams(radSize, radSize));
            }
        }
    }
}
