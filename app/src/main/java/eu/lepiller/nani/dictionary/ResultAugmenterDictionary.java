package eu.lepiller.nani.dictionary;

import java.io.File;

import eu.lepiller.nani.result.Result;

public abstract class ResultAugmenterDictionary extends FileDictionary {
    ResultAugmenterDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    void augment(Result r) throws IncompatibleFormatException {
    }
}
