package eu.lepiller.nani.dictionary;

import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import eu.lepiller.nani.result.Result;

abstract public class ResultDictionary extends FileDictionary {
    final private static String TAG = "JMDICT";
    private Huffman kanjiHuffman, readingHuffman, meaningHuffman;

    ResultDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url,
                     int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    @Override
    public void remove() {
        super.remove();
        kanjiHuffman = null;
        readingHuffman = null;
        meaningHuffman = null;
    }

    class ReadingParser extends Parser<Result.Reading> {
        @Override
        Result.Reading parse(RandomAccessFile file) throws IOException {
            List<String> reading_kanjis = new ListParser<>(new StringParser()).parse(file);
            Log.v(TAG, "kanjis: " + reading_kanjis);
            List<String> reading_infos = new ListParser<>(new StringParser()).parse(file);
            Log.v(TAG, "infos: " + reading_kanjis);
            List<String> reading_readings = new ListParser<>(new HuffmanStringParser(readingHuffman)).parse(file);
            return new Result.Reading(reading_kanjis, reading_infos, reading_readings);
        }
    }

    static class SourceParser extends Parser<Result.Source> {
        @Override
        Result.Source parse(RandomAccessFile file) throws IOException {
            List<String> source_content = new ListParser<>(new StringParser()).parse(file);
            boolean source_wasei = file.read() != 0;
            String source_language = new StringParser().parse(file);
            return new Result.Source(source_content, source_wasei, source_language);
        }
    }

    class SenseParser extends Parser<Result.Sense> {
        @Override
        Result.Sense parse(RandomAccessFile file) throws IOException {
            List<String> sense_references = new ListParser<>(new StringParser()).parse(file);
            List<String> sense_limits = new ListParser<>(new StringParser()).parse(file);
            List<Result.Source> sense_sources = new ListParser<>(new SourceParser()).parse(file);
            List<String> sense_infos = new ListParser<>(new HuffmanStringParser(meaningHuffman)).parse(file);
            List<String> sense_glosses = new ListParser<>(new HuffmanStringParser(meaningHuffman)).parse(file);
            String sense_language = new StringParser().parse(file);
            return new Result.Sense(sense_references, sense_limits, sense_infos, sense_sources,
                    sense_glosses, sense_language);
        }
    }

    class ResultParser extends Parser<Result> {
        @Override
        Result parse(RandomAccessFile file) throws IOException {
            List<String> kanjis = new ListParser<>(new HuffmanStringParser(kanjiHuffman)).parse(file);

            Log.v(TAG, "Getting readings");
            List<Result.Reading> readings = new ListParser<>(new ReadingParser()).parse(file);

            List<Result.Sense> senses = new ListParser<>(new SenseParser()).parse(file);

            int score = file.readByte();
            return new Result(kanjis, readings, senses, score);
        }
    }

    static class ValuesParser extends Parser<List<Integer>> {
        @Override
        List<Integer> parse(RandomAccessFile file) throws IOException {
            ArrayList<Integer> results = new ArrayList<>();

            Log.v(TAG, "Getting values");
            List<Integer> exactResults = new ListParser<>(new Parser<Integer>() {
                @Override
                Integer parse(RandomAccessFile file) throws IOException {
                    return file.readInt();
                }
            }).parse(file);

            List<Integer> others = new ListParser<>(1, new Parser<Integer>() {
                @Override
                Integer parse(RandomAccessFile file) throws IOException {
                    file.skipBytes(1);
                    return file.readInt();
                }
            }).parse(file);

            for(Integer pos: others) {
                file.seek(pos);
                results.addAll(new ValuesParser().parse(file));
            }

            Collections.sort(results);
            Collections.sort(exactResults);

            Log.v(TAG, "exact result size: " + exactResults.size() + ", result size: " + results.size());
            Log.v(TAG, "exact: " + Arrays.toString(exactResults.toArray()) + ", others: " + Arrays.toString(results.toArray()));
            exactResults.addAll(results);
            return exactResults;
        }
    }

    private List<Integer> searchTrie(RandomAccessFile file, long triePos, byte[] txt) throws IOException {
        return searchTrie(file, triePos, txt, 50, new TrieParser<Integer>(new ValuesParser()) {
            @Override
            public void skipVals(RandomAccessFile file, long pos) throws IOException {
                file.seek(pos);
                int valuesLength = file.readShort();
                Log.v(TAG, "number of values: " + valuesLength);
                file.skipBytes(valuesLength * 4);
            }
        });
    }

    List<Result> search(String text) throws IncompatibleFormatException {
        if (isDownloaded()) {
            try {
                RandomAccessFile file = new RandomAccessFile(getFile(), "r");
                byte[] header = new byte[14];
                int l = file.read(header);
                if (l != header.length)
                    return null;

                // Check file format version
                if(!Arrays.equals(header, "NANI_JMDICT003".getBytes())) {
                    StringBuilder error = new StringBuilder("search: incompatible header: [");
                    boolean first = true;
                    for(byte b: header) {
                        if(first)
                            first = false;
                        else
                            error.append(", ");
                        error.append(b);
                    }
                    error.append("].");
                    Log.d(TAG, error.toString());
                    throw new IncompatibleFormatException(getName());
                }

                byte[] search = text.toLowerCase().getBytes();

                file.skipBytes(4); // unused
                long kanjiTriePos = file.readInt();
                long readingTriePos = file.readInt();
                long meaningTriePos = file.readInt();

                Log.d(TAG, "Search in: " + getFile());
                Log.v(TAG, "kanji: " + kanjiTriePos);
                Log.v(TAG, "reading: " + readingTriePos);
                Log.v(TAG, "meaning: " + meaningTriePos);

                kanjiHuffman = new HuffmanParser().parse(file);
                readingHuffman = new HuffmanParser().parse(file);
                meaningHuffman = new HuffmanParser().parse(file);

                logHuffman(readingHuffman, new ArrayList<>());

                // Search in Japanese, by kanji and reading
                Log.d(TAG, "search: by kanji and reading");
                List<Integer> results = searchTrie(file, kanjiTriePos, search);
                List<Integer> readingResults = searchTrie(file, readingTriePos, search);
                if(results != null && readingResults != null)
                    results.addAll(readingResults);
                else if (results == null)
                    results = readingResults;

                Log.d(TAG, "search: by meaning");
                // Search in other language, by meaning
                if(results == null || results.isEmpty())
                    results = searchTrie(file, meaningTriePos, search);

                if(results == null || results.isEmpty())
                    return null;

                Log.d(TAG, results.size() + " result(s)");

                List<Result> r = new ArrayList<>();
                List<Integer> uniqResults = new ArrayList<>();
                for(Integer i: results) {
                    if(!uniqResults.contains(i))
                        uniqResults.add(i);
                }

                Log.v(TAG, Arrays.toString(uniqResults.toArray()));

                int num = 0;
                for(int pos: uniqResults) {
                    if(num > 10)
                        break;
                    num++;
                    file.seek(pos);
                    r.add(new ResultParser().parse(file));
                }
                return r;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
