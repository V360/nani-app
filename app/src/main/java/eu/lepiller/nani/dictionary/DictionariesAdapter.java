package eu.lepiller.nani.dictionary;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.NumberFormat;
import java.util.ArrayList;

import eu.lepiller.nani.R;

public class DictionariesAdapter extends ArrayAdapter<Dictionary> {
    private final Context context;

    public DictionariesAdapter(Context context, ArrayList<Dictionary> dictionaries) {
        super(context, 0, dictionaries);
        this.context = context;
    }

    @Override @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        Dictionary dictionary = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_dictionary, parent, false);
        }

        if (dictionary == null) {
            return convertView;
        }

        // Lookup view for data population
        ImageView icon_view = convertView.findViewById(R.id.icon_view);
        TextView name_view = convertView.findViewById(R.id.name_view);
        TextView description_view = convertView.findViewById(R.id.additional_info_view);
        TextView size_view = convertView.findViewById(R.id.file_size_view);
        TextView entry_count_view = convertView.findViewById(R.id.entries_view);

        // Populate the data into the template view using the data object
        Drawable icon;
        if (dictionary.isDownloaded() && dictionary.canUpdate())
            icon = dictionary.getNewDrawable(context);
        else
            icon = dictionary.getDrawable(context);
        
        if(icon != null) {
            Drawable.ConstantState state = icon.getConstantState();
            if(state != null) {
                icon = state.newDrawable().mutate();
                if (!dictionary.isDownloaded())
                    icon.setAlpha(64);
                else
                    icon.setAlpha(255);
            }
            icon_view.setImageDrawable(icon);
        }

        name_view.setText(dictionary.getName());
        if(dictionary.canUpdate() && dictionary.isDownloaded()) {
            name_view.setTypeface(null, Typeface.BOLD);
        } else {
            name_view.setTypeface(null, Typeface.NORMAL);
        }
        description_view.setText(dictionary.getDescription());

        int size = dictionary.getExpectedFileSize();
        NumberFormat nf = NumberFormat.getInstance();
        if(size < 1500)
            size_view.setText(String.format(context.getString(R.string.dictionary_expected_size_b), nf.format(size)));
        else if(size < 1500000)
            size_view.setText(String.format(context.getString(R.string.dictionary_expected_size_kb), nf.format(size/1000)));
        else
            size_view.setText(String.format(context.getString(R.string.dictionary_expected_size_mb), nf.format(size/1000000)));

        int entries = dictionary.getExpectedEntries();
        entry_count_view.setText(context.getResources().getQuantityString(R.plurals.dico_entry_count, entries, nf.format(entries)));

        // Return the completed view to render on screen
        return convertView;
    }
}
