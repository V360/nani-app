package eu.lepiller.nani.dictionary;

import java.io.File;
import java.util.List;

import eu.lepiller.nani.result.ExampleResult;
import eu.lepiller.nani.result.KanjiResult;

public abstract class ExampleDictionary extends FileDictionary {
    ExampleDictionary(String name, String description, String fullDescription, File cacheDir, File dataDir, String url, int fileSize, int entries, String hash, String lang) {
        super(name, description, fullDescription, cacheDir, dataDir, url, fileSize, entries, hash, lang);
    }

    abstract List<ExampleResult> search(final String word) throws IncompatibleFormatException;
}
